﻿using UnityEngine;
using System.Collections;

public class BackgroundDestroyer : MonoBehaviour {

	private Transform cameraHandle;

	// Initialisation du script
	void Start () {
		cameraHandle = GameObject.Find ("Main Camera").transform;
	}
	
	// Appel de la fonction a chaque frame
	void Update () {
		Vector3 cameraPosition = cameraHandle.position;
		if (transform.position.y < cameraPosition.y - 7.5f) {
			DestroyObject(gameObject);	
		}
	}
}
