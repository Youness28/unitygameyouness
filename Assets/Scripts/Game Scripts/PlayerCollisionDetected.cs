﻿using UnityEngine;
using System.Collections;

public class PlayerCollisionDetected : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D coll) // Si on a collision avec la pierre
	{		
		if (coll.gameObject.name == "obstacle_prefab(Clone)")
		{
			PlayerControlsScript.speed = 0;
			Application.LoadLevel("EndGameScreen");
		}
	}
}
