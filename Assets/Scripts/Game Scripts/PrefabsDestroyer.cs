﻿using UnityEngine;
using System.Collections;

public class PrefabsDestroyer : MonoBehaviour {

	private Transform cameraHandle;

	// Initialisation
	void Start () {
		cameraHandle = GameObject.Find ("Main Camera").transform;
	}
	
	// Appel de la fonction a chaque frame
	void Update () {
		Vector3 cameraPosition = cameraHandle.position;
		if (transform.position.y < cameraPosition.y - 6.0f) {
			DestroyObject(gameObject);	
		}
	}
}
