﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndGameSummary : MonoBehaviour {
	
	public Text endScoreText;

	// Score pour la fin du jeu
	void Start () {
		endScoreText.text = PlayerControlsScript.score.ToString ();
	}
	
	// Appel de la fonction a chaque frame
	void Update () {
	
	}
}
